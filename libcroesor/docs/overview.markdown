### libcroesor Overview

[Mail the maintainers](mailto:rhosydd@apertis.org)

libcroesor is a library for use within the Rhosydd daemon and its backends (both
internal and external backends). It provides utility classes for implementing
collections of attributes and zones in vehicles, and exposing them over D-Bus
to be consumed. The vehicles exposed by Rhosydd backends are consumed by the
Rhosydd daemon itself, which aggregates them and exposes them to applications.

Each backend exposes zero or more vehicles. The main interface for building a
backend is the [](CsrBackend) class, which sets up the necessary D-Bus
connection, and provides a [](CsrVehicleManager) which the backend can use to
expose its vehicles on the bus. Typically, the bulk of the implementation of a
backend will be in building a class which implements `RsdVehicle` to expose
the backend’s vehicle. The [](CsrStaticVehicle) class is provided as a basic
implementation suitable for backends which need to expose vehicles with a fixed
set of attributes whose values do not change over time.

The implementation of an `RsdVehicle` to expose attributes from physical
components in a vehicle depends on how those components are exposed to the
automotive domain. All Rhosydd backends run in the consumer entertainment (CE)
domain so, typically, a Rhosydd backend will need to use the inter-domain
connection to retrieve sensor data from, and to send actuator commands to, the
automotive domain. These implementation details are vehicle-specific; hence all
libcroesor and librhosydd can do is provide the `RsdVehicle` interface to
implement.

Within the daemon, vehicles from the backends are aggregated by their IDs: if
two backends expose a vehicle with the same ID, those two vehicles are exposed
as a single aggregate vehicle by the backend. This means that multiple backends
can be used to provide different attributes for the //same// vehicle, if that
suits the architecture in the automotive domain. For example, one backend could
expose all attributes which require communication over the vehicle’s CAN bus;
and another backend could expose all attributes which are associated with
hardware directly connected to the CE domain. If all these attributes are
exposed on `RsdVehicle` objects with the same ID, they will be aggregated to
form a single vehicle to be exposed to applications. This allows Rhosydd
backends to be compartmentalised for security purposes, rather than requiring a
monolithic backend which has access to all hardware and buses in the vehicle.

When two vehicles are aggregated, their zone hierarchies are combined to form
an aggregate zone hierarchy which includes all zones from both vehicles. If the
vehicles define the same zone, a single instance of that is present in the
aggregate hierarchy (since a zone is a path and nothing more).

Their attribute lists are combined to form an aggregate attribute list: all the
attributes for a given zone from both vehicles are included in the aggregate
attribute list. If both vehicles define the same attribute in a given zone, that
is an error and one or both of the backends providing those vehicles will be
disconnected from the daemon. In future, the system may permit duplicate
attributes to be defined, and would combine their values in the aggregate
attribute list; so do not rely on the current semantics.

The daemon will forward signals from the backends to the aggregated vehicles,
signalling updates to attributes and metadata.

The daemon will also apply security policy to the vehicles it exposes to
applications, reducing the availability of a given attribute if an application
is not permitted to access it. This is pending implementation, and its semantics
will be defined more rigorously in future.
