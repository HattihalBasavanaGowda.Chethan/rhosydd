AC_PREREQ(2.65)

# Release version
m4_define([rsd_version_major],[0])
m4_define([rsd_version_minor],[2020])
m4_define([rsd_version_micro],[1])

# API version
m4_define([rsd_api_version],[0])
m4_define([csr_api_version],[0])

AC_INIT([rhosydd], [rsd_version_major.rsd_version_minor.rsd_version_micro],
        [mailto:maintainers@lists.apertis.org],[rhosydd],
        [https://git.apertis.org/cgit/rhosydd.git/])

AX_CHECK_ENABLE_DEBUG
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_SRCDIR([librhosydd/librhosydd.pc.in])
AC_CONFIG_HEADERS([config.h])
AC_USE_SYSTEM_EXTENSIONS
AM_SILENT_RULES([yes])
AC_REQUIRE_AUX_FILE([tap-driver.sh])

AM_INIT_AUTOMAKE([1.11 -Wno-portability dist-xz no-dist-gzip subdir-objects tar-ustar])

AC_PROG_CXX
AM_PROG_CC_C_O
LT_INIT
PKG_PROG_PKG_CONFIG
AC_PROG_LN_S

AX_SPLIT_VERSION
RSD_VERSION_MAJOR="$AX_MAJOR_VERSION"
RSD_VERSION_MINOR="$AX_MINOR_VERSION"
# Remove nano version or -abcdef or -dirty marker from the micro part
RSD_VERSION_MICRO="`echo "$AX_POINT_VERSION" | $SED 's/[[^0-9]].*//'`"

# Before making a release, the RSD_LT_VERSION string should be modified. The
# string is of the form c:r:a. Follow these instructions sequentially:
#
#  1. If the library source code has changed at all since the last update, then
#     increment revision (‘c:r:a’ becomes ‘c:r+1:a’).
#  2. If any interfaces have been added, removed, or changed since the last
#     update, increment current, and set revision to 0.
#  3. If any interfaces have been added since the last public release, then
#     increment age.
#  4. If any interfaces have been removed or changed since the last public
#     release, then set age to 0.
AC_SUBST([RSD_LT_VERSION],[6:0:0])
AC_SUBST([RSD_VERSION_MAJOR])
AC_SUBST([RSD_VERSION_MINOR])
AC_SUBST([RSD_VERSION_MICRO])
AC_SUBST([RSD_API_VERSION],rsd_api_version)

# Similarly for libcroesor. Use the Rhosydd version numbers for CSR_VERSION_* as
# long as they’re both in the same tarball. libcroesor’s LT and API versions
# remain independent, however. Crucially, CSR_LT_VERSION must be modified
# independently of RSD_LT_VERSION.
AC_SUBST([CSR_LT_VERSION],[5:0:0])
AC_SUBST([CSR_VERSION_MAJOR], [$RSD_VERSION_MAJOR])
AC_SUBST([CSR_VERSION_MINOR], [$RSD_VERSION_MINOR])
AC_SUBST([CSR_VERSION_MICRO], [$RSD_VERSION_MICRO])
AC_SUBST([CSR_API_VERSION],csr_api_version)

# Dependencies
AX_PKG_CHECK_MODULES([RHOSYDD],
                     [glib-2.0 >= 2.58.2 gio-2.0 gobject-2.0],
                     [],
                     [],[],
                     [RSD_PACKAGE_REQUIRES],[RSD_PACKAGE_REQUIRES_PRIVATE])
AX_PKG_CHECK_MODULES([CROESOR],
                     [glib-2.0 >= 2.58.2 gio-2.0 gobject-2.0 canterbury-0 >= 0.10 polkit-gobject-1],
                     [libsystemd],
                     [],[],
                     [CSR_PACKAGE_REQUIRES],[CSR_PACKAGE_REQUIRES_PRIVATE])
AX_PKG_CHECK_MODULES([INTERNAL],
                     [glib-2.0 >= 2.58.2 gio-2.0 gobject-2.0],
                     [],
                     [],[],
                     [INTERNAL_PACKAGE_REQUIRES],[INTERNAL_PACKAGE_REQUIRES_PRIVATE])
AX_PKG_CHECK_MODULES([DAEMON],
                     [glib-2.0 >= 2.58.2 gio-2.0 gobject-2.0],
                     [$CSR_PACKAGE_REQUIRES])
AX_PKG_CHECK_MODULES([BACKENDS_MOCK],
                     [glib-2.0 >= 2.58.2 gio-2.0 gobject-2.0],
                     [yaml-0.1],
                     [$CSR_PACKAGE_REQUIRES])
AX_PKG_CHECK_MODULES([BACKENDS_SPEEDO],
                     [glib-2.0 >= 2.58.2 gio-2.0 gobject-2.0],
                     [json-c],
                     [$CSR_PACKAGE_REQUIRES])
AX_PKG_CHECK_MODULES([CLIENTS],
                     [glib-2.0 >= 2.58.2 gio-2.0 gobject-2.0])
AX_PKG_CHECK_MODULES([DAEMON_TESTS],
                     [glib-2.0 >= 2.58.2 gio-2.0 gobject-2.0])
AX_PKG_CHECK_MODULES([RHOSYDD_TESTS],
                     [glib-2.0 >= 2.58.2 gio-2.0 gobject-2.0])
AX_PKG_CHECK_MODULES([CROESOR_TESTS],
                     [glib-2.0 >= 2.58.2 gio-2.0 gobject-2.0])

# FIXME: There has to be a better way to do this; a new pkg-config macro which
# incorporates intra-package dependencies?
CSR_PACKAGE_REQUIRES="$CSR_PACKAGE_REQUIRES librhosydd-$RSD_API_VERSION"

LT_LIB_M
AC_SUBST([LIBM])

# systemd directories
AC_ARG_WITH([systemdsystemunitdir],
            AS_HELP_STRING([--with-systemdsystemunitdir=DIR],
                           [Directory for systemd service files]),
            [],[with_systemdsystemunitdir='${prefix}/lib/systemd/system'])
AC_SUBST([systemdsystemunitdir],[$with_systemdsystemunitdir])

AC_ARG_WITH([systemdsysusersdir],
            AS_HELP_STRING([--with-systemdsysusersdir=DIR],
                           [Directory for systemd sysusers files]),
            [],[with_systemdsysusersdir='${prefix}/lib/sysusers.d'])
AC_SUBST([systemdsysusersdir],[$with_systemdsysusersdir])

# User to run the daemon as
AC_ARG_WITH([daemon-user],
            [AS_HELP_STRING([--with-daemon-user=<user>],
                            [User for running the Rhosydd daemon (default: rhosydd)])],
            [],[with_daemon_user=rhosydd])

AC_SUBST([DAEMON_USER],[$with_daemon_user])
AC_DEFINE_UNQUOTED([DAEMON_USER],"$with_daemon_user",
                   [User for running the Rhosydd daemon])

# User to run an example backend as
# FIXME: Probably want to remove this for production, as each backend must run
# as a separate user.
AC_ARG_WITH([backend-user],
            [AS_HELP_STRING([--with-backend-user=<user>],
                            [User for running the example Rhosydd backend (default: rhosydd-mock)])],
            [],[with_backend_user=rhosydd-mock])

AC_SUBST([BACKEND_USER],[$with_backend_user])
AC_DEFINE_UNQUOTED([BACKEND_USER],"$with_backend_user",
                   [User for running the example Rhosydd backend])

# Internationalisation
AM_GNU_GETTEXT_VERSION([0.19])
AM_GNU_GETTEXT([external])
AC_DEFINE_UNQUOTED([GETTEXT_PACKAGE],["$PACKAGE"],[gettext package name])

# Code coverage
AX_CODE_COVERAGE

# General macros
AX_IS_RELEASE([dash-version])
AX_COMPILER_FLAGS
AX_VALGRIND_CHECK
AX_GENERATE_CHANGELOG

GOBJECT_INTROSPECTION_CHECK([0.9.7])

AC_PATH_PROG([GLIB_COMPILE_RESOURCES],[glib-compile-resources])

AC_SUBST([AM_CPPFLAGS])
AC_SUBST([AM_CFLAGS])
AC_SUBST([AM_CXXFLAGS])
AC_SUBST([AM_LDFLAGS])

# Documentation
HOTDOC_CHECK([0.8], [c, dbus])

# installed-tests
AC_ARG_ENABLE([always_build_tests],
              AS_HELP_STRING([--enable-always-build-tests],
                             [Enable always building tests (default: yes)]),,
              [enable_always_build_tests=yes])
AC_ARG_ENABLE([installed_tests],
              AS_HELP_STRING([--enable-installed-tests],
                             [Install test programs (default: no)]),,
              [enable_installed_tests=no])

AM_CONDITIONAL([ENABLE_ALWAYS_BUILD_TESTS],
               [test "$enable_always_build_tests" = "yes"])
AC_SUBST([ENABLE_ALWAYS_BUILD_TESTS],[$enable_always_build_tests])

AM_CONDITIONAL([ENABLE_INSTALLED_TESTS],
               [test "$enable_installed_tests" = "yes"])
AC_SUBST([ENABLE_INSTALLED_TESTS],[$enable_installed_tests])

AC_CONFIG_FILES([
Makefile
daemon/tests/Makefile
libcroesor/libcroesor-$CSR_API_VERSION.pc:libcroesor/libcroesor.pc.in
libcroesor/version.h
libcroesor/tests/Makefile
librhosydd/librhosydd-$RSD_API_VERSION.pc:librhosydd/librhosydd.pc.in
librhosydd/version.h
librhosydd/tests/Makefile
po/Makefile.in
tests/Makefile
],[],
[RSD_API_VERSION='$RSD_API_VERSION';CSR_API_VERSION='$CSR_API_VERSION'])
AC_OUTPUT
